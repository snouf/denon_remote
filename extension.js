/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

 //debug note:
 //     journalctl /usr/bin/gnome-session -f -o cat
 //     journalctl -f -o cat
 //     gnome-shell --replace
 //     https://wiki.gnome.org/Projects/GnomeShell/Debugging


const St = imports.gi.St;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;
const Slider = imports.ui.slider
const Lang = imports.lang;
const Soup = imports.gi.Soup;
const _httpSession = new Soup.SessionAsync();
Soup.Session.prototype.add_feature.call(_httpSession, new Soup.ProxyResolverDefault());

const Gio = imports.gi.Gio;

//let aggregateMenuMenu;

//à configurer
const DENON_IP = "192.168.1.12";
const VOL_MAX = 85.;

let inputModeList = [
    //~ { cmd:"SISAT/CBL",      label: "Sat/Cbl",       icon: null },
    //~ { cmd:"DVD",            label: "DVD",           icon: null },
    { cmd:"GAME",           label: "Game",          icon: null },
    //~ { cmd:"USB/IPOD",       label: "USB/Ipod",      icon: null },
    { cmd:"TV",             label: "audio TV",      icon: null },
    { cmd:"TUNER",          label: "FM",            icon: null },
    //~ { cmd:"NET",            label: "Network",       icon: null },
    { cmd:"IRADIO",         label: "Web radio",     icon: null }//,
    //~ { cmd:"FAVORITES",      label: "Favorites",     icon: null }//,
    //~ { cmd:"FLICKR",         label: "Flirck",        icon: null },
    //~ { cmd:"MPLAY",          label: "Mplay",         icon: null },
    //~ { cmd:"PANDORA",        label: "Pandora",      icon: null }, //North America model Only
    //~ { cmd:"RIUSXM",         label: "Riusxm",        icon: null }, //North America model Only
    //~ { cmd:"SPOTIFY",        label: "Spotify",       icon: null }, //North America & Europe model Only
    //~ { cmd:"USB",            label: "USB",           icon: null }, //Select INPUT source iPod/USB and USB Start Playback
    //~ { cmd:"IPD",            label: "Ipod",          icon: null }, //Select INPUT source iPod/USB and iPod Direct Start Playback
    //~ { cmd:"IRP",            label: "Web radio",     icon: null }, //Select INPUT source NETWORK and Internet Radio Start Playback
    //~ { cmd:"FPV",            label: "Favorites",     icon: null }  //Select INPUT source NETWORK and Favorites Start Playback
];

let soundModeList = [
    { cmd: "PSMODE:CINEMA", mode: "PLII Cinema",label: "Movie", icon: null },
    { cmd: "MSSTEREO",      mode: "STEREO",     label: "Music", icon: null },
    { cmd: "PSMODE:GAME",   mode: "PLII Game",  label: "Game",  icon: null },
    { cmd: "MSDIRECT",      mode: "DIRECT",     label: "Pure",  icon: null }
];

let button, i, bin, DenonRemoteVolume, DenonRemoteFuncButtonsBox, DenonRemoteSoundModeButtonsBox;

let DenonRemote;

let Status = {
    //~ _FullUpdate: function(error,callback) {
        //example http://www.1060.org/nk4um/topic/63/
        //~ let xmlDeviceinfo, message;
        //~ var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/Deviceinfo.xml");
        //~ _httpSession.queue_message(request, function(_httpSession, message) {
            //~ if (message.status_code !== 200) {
                //~ log ("DenonRemote: ERROR - " + message.status_code);
                //~ return;
            //~ } else {
                //~ let xmlDeviceinfo = request.response_body.data;
                //~ DenonRemote.Power.label.set_text(findFirstNode(xmlDeviceinfo,'ModelName'));
            //~ }
        //~ });
        //~ this._Update();
    //~ },

    _Update: function() {
        let xmlDeviceinfo, message;
        //~ var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formMainZone_MainZoneXml.xml");
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formMainZone_MainZoneXmlStatusLite.xml");
        //~ var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formNetAudio_StatusXml.xml");
        _httpSession.queue_message(request, function(_httpSession, message) {
            if (message.status_code !== 200) {
                log ("DenonRemote: ERROR - " + message.status_code);
                return;
            } else {
                let xmlStatusLite = request.response_body.data;

                if (findFirstNode(findFirstNode(xmlStatusLite,'Power'),'value') == 'ON') {
                    DenonRemote.Power.setToggleState(true);
                } else {
                    DenonRemote.Power.setToggleState(false);
                }

                let volumeStatus = VOL_MAX+parseFloat(findFirstNode(findFirstNode(xmlStatusLite,'MasterVolume'),'value'));
                DenonRemote.VolumeSlider._updateLabel(volumeStatus);
                DenonRemote.VolumeSlider._updateSlider(volumeStatus)

                //muteStatus == (findFirstNode(findFirstNode(xmlStatusLite,'Mute'),'value') == 'on')

                let InputModeStatus = findFirstNode(findFirstNode(xmlStatusLite,'InputFuncSelect'),'value');
                let InputModeButton;
                log ("DenonRemote: INFO - update InputModeButtons : " + InputModeStatus);
                for each (InputModeButton in DenonRemote.InputModeButtons) {
                    log ("DenonRemote: INFO -    - update button for  : " + InputModeButton.cmd);
                    if (InputModeButton.cmd == InputModeStatus) {
                        InputModeButton.style_class = 'bt-inputmode-active bt-inputmode-'+InputModeButton.cmd.toLowerCase().replace(" ","-")+'-active';
                    } else {
                        InputModeButton.style_class = 'bt-inputmode bt-inputmode-'+InputModeButton.cmd.toLowerCase().replace(" ","-");
                    }
                }
            }
        });

        var request2 = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formMainZone_MainZoneXml.xml");
        _httpSession.queue_message(request2, function(_httpSession, message) {
            if (message.status_code !== 200) {
                log ("DenonRemote: ERROR - " + message.status_code);
                return;
            } else {
                let xmlStatus = request2.response_body.data;
                //~ log ("DenonRemote: INFO - xmlStatus : " + xmlStatus);

                DenonRemote.Power.label.set_text(findFirstNode(findFirstNode(xmlStatus,'FriendlyName'),'value'));

                let SoundModeStatus = findFirstNode(findFirstNode(xmlStatus,'selectSurround'),'value').trim();
                let SoundModeButton;
                log ("DenonRemote: INFO - update SoundModeButtons : " + SoundModeStatus);
                for each (SoundModeButton in DenonRemote.SoundModeButtons) {
                    log ("DenonRemote: INFO -    - update button for  : " + SoundModeButton.mode);
                    if (SoundModeStatus == SoundModeButton.mode) {
                        //~ log ("DenonRemote: INFO - --> yes");
                        SoundModeButton.style_class = 'bt-soundmode-active bt-soundmode-'+SoundModeButton.mode.toLowerCase().replace(" ","-")+'-active';
                    } else {
                        SoundModeButton.style_class = 'bt-soundmode bt-soundmode-'+SoundModeButton.mode.toLowerCase().replace(" ","-");
                    }
                    //~ log ("DenonRemote: INFO - class : "+soundMode.style_class);
                }
            }
        });
    }

};

/*** Widgets ***/

const DenonRemoteButton = new Lang.Class({
    Name: "DenonRemoteButton",
    Extends: St.Button,

    _init: function(bt) {
        this.parent({
                reactive: true,
                can_focus: true,
                track_hover: true
            });
        this.cmd = bt.cmd;
        let icon = new St.Icon({
                icon_name: 'system-run-symbolic',
                style_class: 'system-status-icon'
            });
        this.set_child(icon);

        let label = new St.Label({
                text: bt.label,
                reactive: true
            });
        this.set_child(label);
        this.connect('clicked', Lang.bind(this, this._on_clicked));
    }
})

const InputModeButton = new Lang.Class({
    Name: "DenonRemoteInputModeButton",
    Extends: DenonRemoteButton,

    _init: function(bt) {
        this.parent(bt)
        this.style_class = 'bt-inputmode bt-inputmode-'+this.cmd.toLowerCase().replace(" ","-")
    },

    _on_clicked: function(actor, event) {
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formiPhoneAppDirect.xml?SI" + this.cmd);
        _httpSession.queue_message(request, function(_httpSession, message) {
            if (message.status_code !== 200) {
                log ("DenonRemote: ERROR - " + this.cmd);(message.status_code, null);
                return;
            }
        });

        let InputModeButton;
        for each (InputModeButton in DenonRemote.InputModeButtons) {
            InputModeButton.style_class = 'bt-inputmode bt-inputmode-'+InputModeButton.cmd.toLowerCase().replace(" ","-"); //bt inactif
        }
        this.style_class = 'bt-inputmode-active bt-inputmode-'+this.cmd.toLowerCase().replace(" ","-")+'-active'; //bt actif

        //Status._Update() //FIXME (ajouter timeout)
    }
})

const SoundModeButton = new Lang.Class({
    Name: "DenonRemoteSoundModeButton",
    Extends: DenonRemoteButton,

    _init: function(bt) {
        this.parent(bt);
        this.mode = bt.mode;
        this.style_class = 'bt-soundmode bt-soundmode-'+bt.mode.toLowerCase().replace(" ","-");
    },

    _on_clicked: function(actor, event) {
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formiPhoneAppDirect.xml?" + this.cmd);
        _httpSession.queue_message(request, function(_httpSession, message) {
            if (message.status_code !== 200) {
                log ("DenonRemote: ERROR - " + this.cmd);(message.status_code, null);
                return;
            }
        });

        let SoundModeButton;
        for each (SoundModeButton in DenonRemote.SoundModeButtons) {
            SoundModeButton.style_class = 'bt-soundmode bt-soundmode-'+SoundMode.mode.toLowerCase().replace(" ","-");
        }
        this.style_class = 'bt-soundmode-active bt-soundmode-'+this.mode.toLowerCase().replace(" ","-")+'-active';

        //~ Status._Update() //FIXME (ajouter timeout)
    }
})

const VolumeSlider = new Lang.Class({
    Name: "DenonRemoteVolumeSlider",
    Extends: PopupMenu.PopupBaseMenuItem,

    _init: function(value=0.5) {
        this.parent({reactive: true});
        this._vol = null;

        this._slider = new Slider.Slider(value, {style_class: 'slider-volume'});
        this._icon = new St.Icon({style_class: 'icon-volume', icon_name: 'audio-volume-medium-symbolic'});
        this._label = new St.Label({text: value.toFixed(1), style_class: 'label-volume'});

        this.actor.add(this._icon)
        this.actor.add(this._label)
        this.actor.add(this._slider.actor, {expand: true})

        this._slider.connect('value-changed', Lang.bind(this, this._on_value_changed));
    },

    _on_value_changed: function(actor, event) {
        let vol = Math.round(2*this._slider._value*VOL_MAX)/2;
        if (vol != this._vol) {
            this._vol = vol;
            this._updateLabel();
            this._setDenonVolume();
        }
    },

    //~ _on_button_release_event: function(actor, event) {
        //~ this._setDenonVolume(this._value*VOL_MAX);
    //~ },

    _setDenonVolume: function(vol) {
        let volRel = VOL_MAX-this._vol;
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formiPhoneAppVolume.xml?1+-" + volRel.toFixed(1));
        _httpSession.queue_message(request, function(_httpSession, message) {
            if (message.status_code !== 200) {
                log ("DenonRemote: ERROR - " + message.status_code);
                return;
            }
        });
    },

    _updateLabel: function(vol) {
        if (vol == null) {
            vol = this._vol;
        }
        this._label.set_text(vol.toFixed(1) + ' / ' + VOL_MAX.toFixed(1));
        let relVol = vol/VOL_MAX;
        if (vol < 1) {
            this._icon.set_icon_name('audio-volume-muted-symbolic');
        } else if (relVol < .33) {
            this._icon.set_icon_name('audio-volume-low-symbolic');
        } else if (relVol < .67) {
            this._icon.set_icon_name('audio-volume-medium-symbolic');
        } else {
            this._icon.set_icon_name('audio-volume-high-symbolic');
        }
    },

    _updateSlider: function(vol) {
        if (vol == null) {
            vol = this._vol;
        }
        this._slider.setValue(vol/VOL_MAX);
    }

})


function findFirstNode(xml,tag) {
    let start = xml.indexOf("<"+tag+">")+tag.length+2;
    let end = xml.indexOf("</"+tag+">", start);
    return xml.substring(start,end);
}

function findNodes(xml,tag) {
    let nodes = [];
    let start = xml.indexOf("<"+tag+">")+tag.length+2;
    while (start != -1) {
        end = xml.indexOf("</"+tag+">", start);
        nodes.push(xml.substring(start,end));
        start = xml.indexOf("<"+tag+">",end+tag.length+3);
    }
    return nodes;
}

function on_DenonRemote_open_state_changed(menu,isOpen) {
    if (isOpen) {
        Status._Update();
    }
    return;
}

function on_Power_toggled(button,toggle) {
    if (toggle) {
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formiPhoneAppPower.xml?1+PowerOn");
    } else {
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formiPhoneAppPower.xml?1+PowerStandby");
    }
    _httpSession.queue_message(request, function(_httpSession, message) {
        if (message.status_code !== 200) {
            log ("DenonRemote: ERROR - " + message.status_code);
            return;
        }
    });
}

function init() {

}

function enable() {
    DenonRemote = new PopupMenu.PopupSubMenuMenuItem('DENON');
    DenonRemote.menu.connect('open-state-changed',on_DenonRemote_open_state_changed);

    Main.panel.statusArea.aggregateMenu._DenonRemote = DenonRemote
    Main.panel.statusArea.aggregateMenu.menu.addMenuItem(DenonRemote,8);

    DenonRemote.Power = new PopupMenu.PopupSwitchMenuItem('???');
    DenonRemote.Power.connect('toggled',on_Power_toggled);
    DenonRemote.menu.addMenuItem(DenonRemote.Power);

    let InputModeButtonsItem = new PopupMenu.PopupBaseMenuItem({reactive: true});
    DenonRemote.InputModeButtons = new Array();
    for each (inputMode in inputModeList) {
        let button = new InputModeButton(inputMode);
        DenonRemote.InputModeButtons.push(button);
        InputModeButtonsItem.actor.add(button, {expand: true});
    }
    DenonRemote.menu.addMenuItem(InputModeButtonsItem);

    let SoundModeButtonsItem = new PopupMenu.PopupBaseMenuItem({reactive: true});
    DenonRemote.SoundModeButtons = new Array();
    for each (soundMode in soundModeList) {
        let button = new SoundModeButton(soundMode);
        DenonRemote.SoundModeButtons.push(button);
        SoundModeButtonsItem.actor.add(button, {expand: true});
    }
    DenonRemote.menu.addMenuItem(SoundModeButtonsItem);

    DenonRemote.VolumeSlider = new VolumeSlider();
    DenonRemote.menu.addMenuItem(DenonRemote.VolumeSlider, {expand: true});

    log('DBG DenonRemote : draw finish');

    Status._Update();
}

function disable() {
    DenonRemote.destroy();
}
