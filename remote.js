/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

 //debug: journalctl /usr/bin/gnome-session -f -o cat


const St = imports.gi.St;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;
const Slider = imports.ui.slider
const Lang = imports.lang;
const Soup = imports.gi.Soup;
const _httpSession = new Soup.SessionAsync();
Soup.Session.prototype.add_feature.call(_httpSession, new Soup.ProxyResolverDefault());

const Gio = imports.gi.Gio;

//let aggregateMenuMenu;

//à configurer
const DENON_IP = "192.168.1.12";
const VOL_MAX = 85.;
let funcList = [
    //~ { cmd:"SISAT/CBL",      label: "Sat/Cbl",       icon: null },
    //~ { cmd:"DVD",            label: "DVD",           icon: null },
    { cmd:"GAME",           label: "Game",          icon: null },
    { cmd:"USB/IPOD",       label: "USB/Ipod",      icon: null },
    { cmd:"TV",             label: "audio TV",      icon: null },
    { cmd:"TUNER",          label: "FM",            icon: null },
    //~ { cmd:"NET",            label: "Network",       icon: null },
    { cmd:"IRADIO",         label: "Web radio",     icon: null },
    { cmd:"FAVORITES",      label: "Favorites",     icon: null }//,
    //~ { cmd:"FLICKR",         label: "Flirck",        icon: null },
    //~ { cmd:"MPLAY",          label: "Mplay",         icon: null },
    //~ { cmd:"PANDORA",        label: "Pandora",      icon: null }, //North America model Only
    //~ { cmd:"RIUSXM",         label: "Riusxm",        icon: null }, //North America model Only
    //~ { cmd:"SPOTIFY",        label: "Spotify",       icon: null }, //North America & Europe model Only
    //~ { cmd:"USB",            label: "USB",           icon: null }, //Select INPUT source iPod/USB and USB Start Playback
    //~ { cmd:"IPD",            label: "Ipod",          icon: null }, //Select INPUT source iPod/USB and iPod Direct Start Playback
    //~ { cmd:"IRP",            label: "Web radio",     icon: null }, //Select INPUT source NETWORK and Internet Radio Start Playback
    //~ { cmd:"FPV",            label: "Favorites",     icon: null }  //Select INPUT source NETWORK and Favorites Start Playback
];

let soundModeList = [
    { cmd: "PSMODE:CINEMA", ctrl: "PLII Cinema",label: "Movie", icon: null },
    { cmd: "MSSTEREO",      ctrl: "STEREO",     label: "Music", icon: null },
    { cmd: "PSMODE:GAME",   ctrl: "PLII Game",  label: "Game",  icon: null },
    { cmd: "MSDIRECT",      ctrl: "DIRECT",     label: "Pure",  icon: null }
];

let button, i, bin, denonRemoteVolume, denonRemoteFuncButtonsBox, denonRemoteSoundModeButtonsBox;
//let denonRemoteButtonsList = [];
let denonRemote;

let deviceInfo = {
    fullUpdate: function(error,callback) {
        //example http://www.1060.org/nk4um/topic/63/
        let xmlDeviceinfo, message;
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/Deviceinfo.xml");
        _httpSession.queue_message(request, function(_httpSession, message) {
            if (message.status_code !== 200) {
                log ("DENONREMOTE: ERROR - " + message.status_code);
                return;
            } else {
                let xmlDeviceinfo = request.response_body.data;
                denonRemote.power.label.set_text('DENON' + findFirstNode(xmlDeviceinfo,'ModelName'));

                /*let xmlVolume = findFirstNode(xmlDeviceinfo,'Volume');
                for each (xmlMaxVolItem in findNodes(xmlVolume,('Param'))) {
                    if (findFirstNode(xmlMaxVolItem,'Relative') == DefaultMaxVolumeValue) {
                        VOL_MAX = parseFloat(findFirstNode(xmlMaxVolItem,'Value'));
                        break;
                    }
                }*/
            }
        });
        this.liteUpdate();
    },

    liteUpdate: function() {
        let xmlDeviceinfo, message;
        //~ var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formMainZone_MainZoneXml.xml");
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formMainZone_MainZoneXmlStatusLite.xml");
        //~ var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formNetAudio_StatusXml.xml");
        _httpSession.queue_message(request, function(_httpSession, message) {
            if (message.status_code !== 200) {
                //log ("DENONREMOTE: ERROR - " + message.status_code);
                return;
            } else {
                let xmlStatusLite = request.response_body.data;

                if (findFirstNode(findFirstNode(xmlStatusLite,'Power'),'value') == 'ON') {
                    denonRemote.power.setToggleState(true);
                } else {
                    denonRemote.power.setToggleState(false);
                }

                let volumeStatus = VOL_MAX+parseFloat(findFirstNode(findFirstNode(xmlStatusLite,'MasterVolume'),'value'));
                denonRemoteVolume._updateLabel(volumeStatus);
                denonRemoteVolume._updateSlider(volumeStatus)

                //muteStatus == (findFirstNode(findFirstNode(xmlStatusLite,'Mute'),'value') == 'on')

                let funcStatus = findFirstNode(findFirstNode(xmlStatusLite,'InputFuncSelect'),'value');
                //log ("DENONREMOTE: INFO - denonRemoteFuncButtonsBox.get_children()");
                for each (func in denonRemoteFuncButtonsBox.get_children()) {
                    //log ("DENONREMOTE: INFO - funcStatus : " + funcStatus + " ?= " + func.cmd);
                    if (func.cmd == funcStatus) {
                        func.style_class = 'bt-zone-active bt-zone-'+func.cmd.toLowerCase().replace(" ","-")+'-active';
                    } else {
                        func.style_class = 'bt-zone bt-zone-'+func.cmd.toLowerCase().replace(" ","-");
                    }
                }
            }
        });

        var request2 = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formMainZone_MainZoneXml.xml");
        _httpSession.queue_message(request2, function(_httpSession, message) {
            if (message.status_code !== 200) {
                log ("DENONREMOTE: ERROR - " + message.status_code);
                return;
            } else {
                let xmlStatus = request2.response_body.data;
                //~ log ("DENONREMOTE: INFO - xmlStatus : " + xmlStatus);

                denonRemote.power.label.set_text(findFirstNode(findFirstNode(xmlStatus,'FriendlyName'),'value'));

                let soundModeStatus = findFirstNode(findFirstNode(xmlStatus,'selectSurround'),'value').trim();
                for each (soundMode in denonRemoteSoundModeButtonsBox.get_children()) {
                    //~ log ("DENONREMOTE: INFO - soundMode : " + soundMode.ctrl + " ?in " + soundModeStatus);
                    if (soundModeStatus == soundMode.ctrl) {
                        //~ log ("DENONREMOTE: INFO - --> yes");
                        soundMode.style_class = 'bt-soundmode-active bt-soundmode-'+soundMode.ctrl.toLowerCase().replace(" ","-")+'-active';
                    } else {
                        soundMode.style_class = 'bt-soundmode bt-soundmode-'+soundMode.ctrl.toLowerCase().replace(" ","-");
                    }
                    //~ log ("DENONREMOTE: INFO - class : "+soundMode.style_class);
                }
            }
        });
    }

};

/*** Widgets ***/

const denonRemoteButton = new Lang.Class({
    Name: "denonRemoteButton",
    Extends: St.Button,

    _init: function(bt) {
        this.parent({
                reactive: true,
                can_focus: true,
                track_hover: true
            });
        this.cmd = bt.cmd;
        let icon = new St.Icon({
                icon_name: 'system-run-symbolic',
                style_class: 'system-status-icon'
            });
        this.set_child(icon);

        let label = new St.Label({
                text: bt.label,
                reactive: true
            });
        this.set_child(label);
        this.connect('clicked', Lang.bind(this, this._on_clicked));
    }
})

const denonRemoteFuncButton = new Lang.Class({
    Name: "denonRemoteFuncButton",
    Extends: denonRemoteButton,

    _init: function(bt) {
        this.parent(bt)
        this.style_class = 'bt-zone bt-zone-'+this.cmd.toLowerCase().replace(" ","-")
    },

    _on_clicked: function(actor, event) {
        //log ("DENONREMOTE: INFO - denonRemoteFuncButton.denonRemoteFuncButtonsBox.get_children()");
        for each (func in denonRemoteFuncButtonsBox.get_children()) {
            func.style_class = 'bt-zone bt-zone-'+func.cmd.toLowerCase().replace(" ","-");
        }
        this.style_class = 'bt-zone-active bt-zone-'+this.cmd.toLowerCase().replace(" ","-")+'-active';
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formiPhoneAppDirect.xml?SI" + this.cmd);
        _httpSession.queue_message(request, function(_httpSession, message) {
            if (message.status_code !== 200) {
                log ("DENONREMOTE: ERROR - " + this.cmd);(message.status_code, null);
                return;
            }
        });
    }
})

const denonRemoteSoundModeButton = new Lang.Class({
    Name: "denonRemoteSoundModeButton",
    Extends: denonRemoteButton,

    _init: function(bt) {
        this.parent(bt)
        this.ctrl = bt.ctrl
        this.style_class = 'bt-soundmode bt-soundmode-'+this.cmd.toLowerCase().replace(" ","-")
    },

    _on_clicked: function(actor, event) {
        //log ("DENONREMOTE: INFO - denonRemoteSoundModeButton.denonRemoteSoundModeButtonsBox.get_children()");
        for each (soundMode in denonRemoteSoundModeButtonsBox.get_children()) {
            soundMode.style_class = 'bt-soundmode bt-soundmode-'+soundMode.ctrl.toLowerCase().replace(" ","-");
        }
        this.style_class = 'bt-soundmode-active bt-soundmode-'+this.ctrl.toLowerCase().replace(" ","-")+'-active';
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formiPhoneAppDirect.xml?" + this.cmd);
        _httpSession.queue_message(request, function(_httpSession, message) {
            if (message.status_code !== 200) {
                log ("DENONREMOTE: ERROR - " + this.cmd);(message.status_code, null);
                return;
            }
        });
    }
})

const denonRemoteSlider = new Lang.Class({
    Name: "denonRemoteSlider",
    Extends: PopupMenu.PopupBaseMenuItem,

    _init: function(value) {
        this.parent();
        this._volMax = VOL_MAX;
        this._vol = null;


        //this.actor.remove(this._slider);
        this._box = new St.Table({style_class: 'slider-item'});
        this._slider = new Slider.Slider(value);
        this._icon = new St.Icon({style_class: 'icon-volume', icon_name: 'audio-volume-medium-symbolic'});
        //~ this._slider.style_class = 'slider-volume'
        this._label = new St.Label({text: value.toFixed(1)});

        this._box.add(this._icon,   {row: 0, col: 0, x_expand: false, y_expand: false})
        this._box.add(this._label,  {row: 0, col: 1, x_expand: false, y_expand: false})
        this._box.add(this._slider.actor, {row: 0, col: 2, x_expand: true})

        denonRemoteSoundModeButtonsBox = new St.BoxLayout({style_class: 'soundMode'});
        //log ("DENONREMOTE: INFO - denonRemoteSlider.soundModeList");
        for each (soundMode in soundModeList) {
            denonRemoteSoundModeButtonsBox.add_actor(new denonRemoteSoundModeButton(soundMode));
        }
        this._box.add(denonRemoteSoundModeButtonsBox, {row: 1, col: 2})

        this.actor.add(this._box, {span: -1, expand: true});
        this.connect('value-changed', Lang.bind(this, this._on_value_changed));
    },

    _on_value_changed: function(actor, event) {
        let vol = Math.round(2*this._value*this._volMax)/2;
        if (vol != this._vol) {
            this._vol = vol;
            this._updateLabel();
            this._setDenonVolume();
        }
    },

    _on_button_release_event: function(actor, event) {
        this._setDenonVolume(this._value*this._volMax);
    },

    _setDenonVolume: function(vol) {
        let volRel = this._volMax-this._vol;
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formiPhoneAppVolume.xml?1+-" + volRel.toFixed(1));
        _httpSession.queue_message(request, function(_httpSession, message) {
            if (message.status_code !== 200) {
                log ("DENONREMOTE: ERROR - " + message.status_code);
                return;
            }
        });
    },

    _updateLabel: function(vol) {
        if (vol == null) {
            vol = this._vol;
        }
        this._label.set_text(vol.toFixed(1) + ' / ' + this._volMax.toFixed(1));
        let relVol = vol/this._volMax;
        if (vol < 1) {
            this._icon.set_icon_name('audio-volume-muted-symbolic');
        } else if (relVol < .33) {
            this._icon.set_icon_name('audio-volume-low-symbolic');
        } else if (relVol < .67) {
            this._icon.set_icon_name('audio-volume-medium-symbolic');
        } else {
            this._icon.set_icon_name('audio-volume-high-symbolic');
        }
    },

    _updateSlider: function(vol) {
        if (vol == null) {
            vol = this._vol;
        }
        this.setValue(vol/this._volMax);
    }

})


function findFirstNode(xml,tag) {
    let start = xml.indexOf("<"+tag+">")+tag.length+2;
    let end = xml.indexOf("</"+tag+">", start);
    return xml.substring(start,end);
}

function findNodes(xml,tag) {
    let nodes = [];
    let start = xml.indexOf("<"+tag+">")+tag.length+2;
    while (start != -1) {
        end = xml.indexOf("</"+tag+">", start);
        nodes.push(xml.substring(start,end));
        start = xml.indexOf("<"+tag+">",end+tag.length+3);
    }
    return nodes;
}

function on_denonRemote_open_state_changed(menu,isOpen) {
    if (isOpen) {
        deviceInfo.liteUpdate();
        //~ deviceInfo.fullUpdate();
    }
    return;
}

function on_denonRemote_OnOff_toggled(button,toggle) {
    if (toggle) {
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formiPhoneAppPower.xml?1+PowerOn");
    } else {
        var request = Soup.Message.new('GET', "http://" + DENON_IP + "/goform/formiPhoneAppPower.xml?1+PowerStandby");
    }
    _httpSession.queue_message(request, function(_httpSession, message) {
        if (message.status_code !== 200) {
            log ("DENONREMOTE: ERROR - " + message.status_code);
            return;
        }
    });
}

function init() {

}

function enable() {
    denonRemote = new PopupMenu.PopupSubMenuMenuItem('DENON');
    denonRemote.connect('open-state-changed',on_denonRemote_open_state_changed);

    Main.panel.statusArea.aggregateMenu._denonRemote = denonRemote
    Main.panel.statusArea.aggregateMenu.menu.addMenuItem(denonRemote,8);

    denonRemote.power = new PopupMenu.PopupSwitchMenuItem('DENON');
    denonRemote.power.connect('toggled',on_denonRemote_OnOff_toggled);
    denonRemote.menu.addMenuItem(denonRemote.power);

    let denonRemoteButtons = new PopupMenu.PopupBaseMenuItem({reactive: false});
    denonRemoteFuncButtonsBox = new St.BoxLayout({style_class: 'zones'});
    //log ("DENONREMOTE: INFO - enable.funcList");
    for each (func in funcList) {
        //log ('to');
        denonRemoteFuncButtonsBox.add_actor(new denonRemoteFuncButton(func));
    }
    denonRemoteButtons.actor.add(denonRemoteFuncButtonsBox);
    denonRemote.menu.addMenuItem(denonRemoteButtons);

    //log ("DENONREMOTE: INFO - enable.volumSlider");
    denonRemoteVolume = new denonRemoteSlider(.5);
    denonRemote.menu.addMenuItem(denonRemoteVolume);

    let denonRemoteButtons = new PopupMenu.PopupBaseMenuItem({reactive: false});
    denonRemoteSoundModeButtonsBox = new St.BoxLayout({style_class: 'soundMode'});
    //log ("DENONREMOTE: INFO - enable.soundMode");
    for each (soundMode in soundModeList) {
        //log ("soundMode:");
        denonRemoteSoundModeButtonsBox.add_actor(new denonRemoteSoundModeButton(soundMode));
    }
    denonRemoteButtons.addActor(denonRemoteSoundModeButtonsBox);
    denonRemote.menu.addMenuItem(denonRemoteButtons)

    deviceInfo.fullUpdate();
}

function disable() {
    denonRemote.destroy();
}
