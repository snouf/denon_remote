/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

const Lang = imports.lang;
const PopupMenu = imports.ui.popupMenu;
const Slider = imports.ui.slider;
const St = imports.gi.St;
const Volume = imports.ui.status.volume;


const VolumeSlider = new Lang.Class({
    Name: 'DenonRemoteVolumeSlider',
    Extends: PopupMenu.PopupSubMenuMenuItem,

    _init: function(value) {
        this.parent();
        this._value = value;
        this._box = new St.Table({style_class: 'DenonVolumeSlider-item'});
        this._slider = new Slider.Slider(value);
        this._icon = new St.Icon({style_class: 'DenonVolumeSlider-icon-volume', icon_name: 'audio-volume-medium-symbolic'});
        //~ this._label = new St.Label({text: value.toFixed(1)});

        this._box.add(this._icon,   {row: 0, col: 0, x_expand: false, y_expand: false})
        this._box.add(this._slider.actor, {row: 0, col: 1, x_expand: true})

        //~ this._box.add(this._label,  {row: 0, col: 1, x_expand: false, y_expand: false})
        //~ this._box.add(this._slider.actor, {row: 0, col: 2, x_expand: true})

        this.connect('value-changed', Lang.bind(this, this._on_value_changed));
    },

    _updateSliderIcon: function() {
        if (this._value < 1) {
            this._icon.set_icon_name('audio-volume-muted-symbolic');
        } else if (this._value < .33) {
            this._icon.set_icon_name('audio-volume-low-symbolic');
        } else if (this._value < .67) {
            this._icon.set_icon_name('audio-volume-medium-symbolic');
        } else {
            this._icon.set_icon_name('audio-volume-high-symbolic');
        }
    }
})


const Menu = new Lang.Class({
    Name: 'DenonRemoteMenu',
    Extends: PopupMenu.PopupMenu,

    _init: function(items) {
        this.parent();
    }
})


const MenuItem = new Lang.Class({
    Name: 'DenonRemoteMenuItem',
    Extends: PopupMenu.PopupMenuItem,

    _init: function(text, cmd) {
        this.parent(text);
        this._cmd = cmd;
    }
})
