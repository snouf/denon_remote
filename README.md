# Denon Remote (gnome shell extension)

![Printscreen](http://jopc.free.fr/ftp/images/2014-10-12_-_19-40-32_-_capture_du_2014_10_12_21_38_18.png)

Control your denon AVR from gnome shell. Install see [Wiki](https://gitlab.com/snouf/denon_remote/wikis/home).